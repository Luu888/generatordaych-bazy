﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Generator_danych
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        String[] imiona = new string[] { "Tomasz", "Jan", "Paweł", "Andrzej",
"Piotr",
"Krzysztof",
"Stanisław",
"Józef",
"Marcin",
"Marek",
"Michał",
"Grzegorz",
"Jerzy",
"Tadeusz",
"Adam",
"Łukasz",
"Zbigniew",
"Ryszard",
"Dariusz",
"Henryk",
"Mariusz",
"Kazimierz",
"Wojciech",
"Robert",
"Mateusz",
"Marian",
"Rafał",
"Jacek",
"Janusz",
"Mirosław",
"Maciej",
"Sławomir",
"Jarosław",
"Kamil",
"Wiesław",
"Roman",
"Władysław",
"Jakub",
"Artur",
"Zdzisław",
"Edward",
"Mieczysław",
"Damian",
"Dawid",
"Przemysław",
"Sebastian",
"Czesław",
"Leszek",
"Daniel",
"Waldemar"};
        String[] nazwiska = new string[]
         {
"Kowalski",
"Nowak",
"Wodecki",
"Marczak",
"Wolski",
"Ignacy",
"Krawczyk",
"Kosek",
"Kozub",
"Kukulski",
"Wiench",
"Marciniszyn",
"Karcinkowski",
"Flak",
"Wolicki",
"Kasperkiewicz",
"Mrozik",
"Rozik",
"Pan",
"Kurek",
"Bobrowski",
"Olender",
"Janik",
"Wabnic",
"Paluch",
"Owca",
"Kubaczyński",
"Ospa",
"Malczak",
"Walczak",
"Florczak",
"Wrzask",
"Makuszycki",
"Nowy",
"Stary",
"Piekarz",
"Piekarczyk",
"Szpak",
"Strzelczyk",
"Gorlicki",
"Janowski",
"Zozol",
"Zdun",
"Mirowski",
"Lewandowski",
"Mikulski",
"Zawodny",
"Niezawodny",
"Niewiadomski",
"Jonek"
         };

        private void button1_Click(object sender, EventArgs e)
            {
                string connString = "DATA SOURCE=217.173.198.135:1522/orcltp.iaii.local;" + "USER ID=s93209;PASSWORD=s93209;";
                OracleConnection conn = new OracleConnection(connString);
                conn.ConnectionString = connString;
                conn.Open();
                string komenda = "select max(numer_kelnera) from kelner";
                OracleCommand cmd = new OracleCommand(komenda);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                OracleDataReader reader = cmd.ExecuteReader();
                string zmienna = "";
                while (reader.Read())
                {
                    if (Convert.IsDBNull(reader.GetValue(0)))
                    {
                        zmienna = "0";
                    }
                    else
                    {
                        zmienna = reader.GetDecimal(0).ToString();
                    }
                }
                reader.Dispose();
                cmd.Dispose();
                conn.Dispose();
                conn.Close();
                int koncowy_id = Convert.ToInt32(zmienna);
                Random rnd = new Random();
                int liczba_wierszy = Convert.ToInt32(textBox1.Text);
                for (int i = 0; i < liczba_wierszy; i++)
                {
                    int rand_imie = rnd.Next(0, 50);
                    int rand_nazwisko = rnd.Next(0, 50);
                    richTextBox1.Text += "INSERT INTO \"S93209\".\"KELNER\" (NUMER_KELNERA, IMIE_KELNERA, NAZWISKO_KELNERA) VALUES ('" + (koncowy_id + 1) + "', '"+imiona[rand_imie]+"', '"+ nazwiska[rand_nazwisko]+"');" + Environment.NewLine;
                    koncowy_id++;
                }
                string path1 = Directory.GetCurrentDirectory();
                string path = path1 + @"\kelner.sql";
                if (!File.Exists(path))
                {
                    var file = File.Create(path);
                    file.Close();
                }
                if (File.Exists(path))
                {
                    TextWriter tw = new StreamWriter(path, true);
                    tw.Write(richTextBox1.Text);
                    tw.Close();
                }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
