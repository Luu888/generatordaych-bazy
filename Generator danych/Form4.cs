﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Generator_danych
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }
        int[] ilosc_miejsc = new int[] { 2, 4, 5, 6, 8 };

        private void button1_Click(object sender, EventArgs e)
        {
            string connString = "DATA SOURCE=217.173.198.135:1522/orcltp.iaii.local;" + "USER ID=s93209;PASSWORD=s93209;";
            OracleConnection conn = new OracleConnection(connString);
            conn.ConnectionString = connString;
            conn.Open();
            string komenda = "select max(numer_stolika) from stolik";
            OracleCommand cmd = new OracleCommand(komenda);
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            OracleDataReader reader = cmd.ExecuteReader();
            string zmienna = "";
            while (reader.Read())
            {
                if (Convert.IsDBNull(reader.GetValue(0)))
                {
                    zmienna = "0";
                }
                else
                {
                    zmienna = reader.GetDecimal(0).ToString();
                }
            }
            reader.Dispose();
            cmd.Dispose();
            conn.Dispose();
            conn.Close();
            int koncowy_id = Convert.ToInt32(zmienna);
            Random rnd = new Random();
            int liczba_wierszy = Convert.ToInt32(textBox1.Text);
            for (int i = 0; i < liczba_wierszy; i++)
            {
                int rand_liczba_miejsc = rnd.Next(0, 5);
                int rand_kelner_id = rnd.Next(1, 10);
                //richTextBox1.Text += "INSERT INTO \"S93209\".\"STOLIK\" (NUMER_STOLIKA, ILOSC_MIEJSC, FK_NUMER_KELNERA) VALUES ('" + (i + 1).ToString() + "', '" + ilosc_miejsc[rand_liczba_miejsc] + "', '" + rand_kelner_id + "');" + Environment.NewLine;
                richTextBox1.Text += "INSERT INTO \"S93209\".\"STOLIK\" (NUMER_STOLIKA, ILOSC_MIEJSC, FK_NUMER_KELNERA) VALUES ('" + (koncowy_id + 1) + "', '" + ilosc_miejsc[rand_liczba_miejsc] + "', '" + rand_kelner_id + "');" + Environment.NewLine;
                koncowy_id++;
            }
            string path1 = Directory.GetCurrentDirectory();
            string path = path1 + @"\stolik.sql";
            if (!File.Exists(path))
            {
                var file = File.Create(path);
                file.Close();
            }
            if (File.Exists(path))
            {
                TextWriter tw = new StreamWriter(path, true);
                tw.Write(richTextBox1.Text);
                tw.Close();
            }
            //}
            //else
            //{
            //    File.Create(path);
            //    TextWriter tw = new StreamWriter(path);
            //    tw.Write(richTextBox1.Text);
            //    tw.Close();
            //}
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void Form4_Load(object sender, EventArgs e)
        {

        }
    }
}
