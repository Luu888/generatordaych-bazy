﻿using Oracle.ManagedDataAccess.Client;
using Oracle;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Generator_danych
{
    public partial class Form6 : Form
    {
        public Form6()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            string connString = "DATA SOURCE=217.173.198.135:1522/orcltp.iaii.local;" + "USER ID=s93209;PASSWORD=s93209;";
            OracleConnection conn = new OracleConnection(connString);
            conn.ConnectionString = connString;
            conn.Open();
            string komenda = "select max(numer_rezerwacji) from rezerwacja";
            OracleCommand cmd = new OracleCommand(komenda);
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            OracleDataReader reader = cmd.ExecuteReader();
            string zmienna ="";
            while (reader.Read())
            {
                if (Convert.IsDBNull(reader.GetValue(0)))
                {
                    zmienna = "0";
                }
                else
                {
                    zmienna = reader.GetDecimal(0).ToString();
                }
            }
            reader.Dispose();
            cmd.Dispose();
            conn.Dispose();
            conn.Close();
            int koncowy_id = Convert.ToInt32(zmienna);
            Random rnd = new Random();
            int liczba_wierszy = Convert.ToInt32(textBox1.Text);
            for (int i = 0; i < liczba_wierszy; i++)
            {
                int rand_numer_stolika = rnd.Next(1, 10);
                Random gen = new Random();
                int range = 2 * 365;  
                DateTime randomDate = DateTime.Today.AddDays(gen.Next(range)).AddMinutes(gen.Next(range));
                richTextBox1.Text += "INSERT INTO \"S93209\".\"REZERWACJA\" (NUMER_REZERWACJI, DATA_REZERWACJI, FK_NUMER_STOLIKA, FK_NUMER_KLIENTA) VALUES ( '"+(koncowy_id+1)+"', '"+ randomDate.ToString("yyyy-MM-dd HH:mm") + "', '" +rand_numer_stolika+ "', '" + (i + 1).ToString()+ "');" + Environment.NewLine;
                int ile_potraw = rnd.Next(1, 8);
                for (int j = 0; j <= ile_potraw; j++)
                    {
                        richTextBox1.Text += "INSERT INTO \"S93209\".\"REZERWACJA_POTRAWA\" (FK_NUMER_REZERWACJI, FK_NUMER_POTRAWY) VALUES ('" + (koncowy_id + 1) + "', '" + rnd.Next(1, 30) + "');" + Environment.NewLine;
                    }
                koncowy_id++;
            }
            string path1 = Directory.GetCurrentDirectory();
            string path = path1 + @"\rezerwacja.sql";
            if (!File.Exists(path))
            {
                var file = File.Create(path);
                file.Close();
            }
            if (File.Exists(path))
            {
                TextWriter tw = new StreamWriter(path, true);
                tw.Write(richTextBox1.Text);
                tw.Close();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_press(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void Form6_Load(object sender, EventArgs e)
        {

        }
    }
}
