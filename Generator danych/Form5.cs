﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Generator_danych
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int liczba_wierszy = Convert.ToInt32(textBox1.Text);
            for (int i = 0; i < liczba_wierszy; i++)
            {
                richTextBox1.Text += "INSERT INTO \"S93209\".\"REZERWACJA_POTRAWA\" (FK_NUMER_REZERWACJI, FK_NUMER_POTRAWY) VALUES ('" + (i + 1).ToString() + "', '" + (i + 1).ToString() +"');" + Environment.NewLine;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
