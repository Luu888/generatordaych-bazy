﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Generator_danych
{
    public partial class Form7 : Form
    {
        public Form7()
        {
            InitializeComponent();
        }
        String[] nazwy1 = new string[]
{
"Ryz",
"Kasza",
"Ziemniaki",
"Frytki",
"Makaron",
"Naleśnik"
};
        String[] nazwy2 = new string[]
{
"+ kurczak gotowany",
"+ kurczak w panierce",
"+ rolada drobiowa",
"+ kotlet schabowy",
};
        String[] nazwy3 = new string[]
{
"+ buraczki",
"+ surowka colesław",
"+ surowka wiosenna",
"+ surowka po grecku",
};

        private void button1_Click(object sender, EventArgs e)
        {
            string connString = "DATA SOURCE=217.173.198.135:1522/orcltp.iaii.local;" + "USER ID=s93209;PASSWORD=s93209;";
            OracleConnection conn = new OracleConnection(connString);
            conn.ConnectionString = connString;
            conn.Open();
            string komenda = "select max(numer_potrawy) from potrawa";
            OracleCommand cmd = new OracleCommand(komenda);
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            OracleDataReader reader = cmd.ExecuteReader();
            string zmienna = "";
            while (reader.Read())
            {
                if (Convert.IsDBNull(reader.GetValue(0)))
                {
                    zmienna = "0";
                }
                else
                {
                    zmienna = reader.GetDecimal(0).ToString();
                }
            }
            reader.Dispose();
            cmd.Dispose();
            conn.Dispose();
            conn.Close();
            int koncowy_id = Convert.ToInt32(zmienna);
            Random rnd = new Random();
            int liczba_wierszy = Convert.ToInt32(textBox1.Text);
            for (int i = 0; i < liczba_wierszy; i++)
            {
                int rand_nazwa = rnd.Next(0, 6);
                int rand_nazwa1 = rnd.Next(0, 4);
                int rand_nazwa2 = rnd.Next(0, 4);
                int cena = rnd.Next(10, 40);

                richTextBox1.Text += "INSERT INTO \"S93209\".\"POTRAWA\" (NUMER_POTRAWY, NAZWA_POTRAWY, CENA_POTRAWY) VALUES ('" + (koncowy_id + 1) + "', '" +  nazwy1[rand_nazwa] + nazwy2[rand_nazwa1] + nazwy3[rand_nazwa2] + "', '" +cena+ "');" + Environment.NewLine;
                koncowy_id++;
            }
            string path1 = Directory.GetCurrentDirectory();
            string path = path1 + @"\potrawa.sql";
            if (!File.Exists(path))
            {
                var file = File.Create(path);
                file.Close();
            }
            if (File.Exists(path))
            {
                TextWriter tw = new StreamWriter(path, true);
                tw.Write(richTextBox1.Text);
                tw.Close();
            }
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void text_box(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void press(object sender, KeyPressEventArgs e)
        {
           
        }
    }
}
    
